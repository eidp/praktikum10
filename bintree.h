#ifndef BINTREE_H
#define BINTREE_H

#include <iostream>
#include <fstream>
#include <string>
#include "eidpliste.h"

using namespace std;

template<typename T>
class BinTree {
private:
    struct Node {
        T data;
        Node *left, *right; // linker und rechter Unterbaum
    };
    
    Node* root; // Wurzel
    
    bool isElement(Node* node, T data) {
        if (node == nullptr)
            return false;
        if (node->data == data)
            return true;
        if (node->data < data)
            return isElement(node->right, data);
        return isElement(node->left, data);
    }
    
    void clear(Node* node) {
        if (node != nullptr) {
            clear(node->left);
            clear(node->right);
            delete node;
        }
    }
    
    Node* insert(Node *node, T data) {
        if (node == nullptr) {
            node = new Node;
            node->data = data;
            node->left = node->right = nullptr;
            return node;
        }
        if (node->data < data)
            node->right = insert(node->right, data);
        else if (node->data > data)
            node->left = insert(node->left, data);
        return node;
    }
    
    // Aufgabe 1a)
    void preOrder(Node* node) {
        if (node == nullptr)
            return;
        
        cout << node->data << " ";
        preOrder(node->left);
        preOrder(node->right);
    }
    
    // Aufgabe 1a)
    void inOrder(Node* node) {
        if (node == nullptr)
            return;
        
        inOrder(node->left);
        cout << node->data << " ";
        inOrder(node->right);
    }
    
    // Aufgabe 1a)
    void postOrder(Node* node) {
        if (node == nullptr)
            return;
        
        postOrder(node->left);
        postOrder(node->right);
        cout << node->data << " ";
    }
    
    // Aufgabe 1b)
    int height(Node* node) {
        if (node == nullptr)
            return 0;
        
        int l_h = height(node->left);
        int r_h = height(node->right);

        return 1 + ((l_h > r_h) ? l_h : r_h);
    }
    
    // Aufgabe 1c)
    int count(Node* node){
        if (node == nullptr)
            return 0;
        
        return count(node->left) + count(node->right) + 1;
    }
    
    // Aufgabe 1d)
    void range(Node* node, T min, T max){
        if (node == nullptr)
            return;
        
        if (min < node->data)
            range(node->left, min, max);
        if (min <= node->data && node->data <= max)
            cout << node->data << " ";
        if (node->data < max)
            range(node->right, min, max);
    }

    // Aufgabe 1f)
    void save_rec(Node* node, ofstream &f) {
        if (node == nullptr)
            return;
        
        f << node->data << ",";
        save_rec(node->left, f);
        save_rec(node->right, f);
    }
    
public:
    BinTree() : root(nullptr) {
    }
    
    ~BinTree() {
        clear(root);
    }
    
    void clear() {
        clear(root);
        root = nullptr;
    }
    
    void insert(T x) {
        root = insert(root, x);
    }
    
    bool isElement(T x) {
        return isElement(root, x);
    }
    
    T rootData(){
        return root->data;
    }
    
    void preOrder() {
        preOrder(root);
        cout << endl;
    }
    
    void inOrder() {
        inOrder(root);
        cout << endl;
    }
    
    void postOrder() {
        postOrder(root);
        cout << endl;
    }
    
    int height() {
        return height(root);
    }
    
    int count() {
        return count(root);
    }
    
    void range(T min, T max){
        range(root, min, max);
    }
    
    // Aufgabe 1e)
    void rotateLeft() {
        if (root == nullptr || root->left == nullptr)
            return;
        
        Node* moriturus = root;
        Node* opfer_anode = root->left->right;
        root = root->left;
        root->right = moriturus;
        moriturus->left = opfer_anode;
    }

    void save(ofstream &f) {
        save_rec(root, f);
    }
};



#endif /* BINTREE_H */
